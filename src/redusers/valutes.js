function pushValuteTransaction(state, transaction) {
    if(state.length > 9) state.splice(0,1);
    state.push({
        datetime: new Date(transaction.Date).toLocaleString(),
        EUR: transaction.Valute.EUR.Value,
        USD: transaction.Valute.USD.Value,
    });
    return state;
}

export function valutes(state = [], action) {
    switch (action.type) {
        case 'VALUTES_FETCH_DATA_SUCCESS': 
            return pushValuteTransaction(state, action.valutes) ;
        default:
            return state;
    }
}