
import React, { Component } from 'react';
import { LineChart, XAxis, YAxis, CartesianGrid, Line } from 'recharts';

class MainChart extends Component {
    render() {
        return <div className="valutes-graf">
            <LineChart width={1000} height={300} data={this.props.data}>
                <XAxis dataKey="datetime"/>
                <YAxis/>
                <CartesianGrid stroke="#eee" strokeDasharray="5 5"/>
                <Line type="monotone" dataKey="EUR" stroke="#8884d8" />
                <Line type="monotone" dataKey="USD" stroke="#82ca9d" />
            </LineChart>
        </div>
    }
}

export default MainChart