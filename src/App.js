import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { valutesFetchData } from './actions/valutes';
import MainChart from './components/MainChart.js';

class App extends Component {  
  constructor() {
    super();
    this.state = { 
      date: new Date(),
    };
  }

  tick() {
    this.props.fetchData('https://www.cbr-xml-daily.ru/daily_json.js');
    this.setState({
      date: new Date()
    });
  }

  componentDidMount() {  
      this.timerID = setInterval(() => this.tick(),1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  render() {
    let titlePlaceholder;
    let chartPlaceholder;

    if(this.props.valutes.length) {
      titlePlaceholder = <div className='valutes-title'>
        <p><b>EUR: </b>{this.props.valutes[this.props.valutes.length - 1].EUR} rub.</p>
        <p><b>USD: </b>{this.props.valutes[this.props.valutes.length - 1].USD} rub.</p>
      </div>
    }else{
      titlePlaceholder = <p>API data not yet received</p>
    }

    if(this.props.valutes.length >= 10) {
      chartPlaceholder = <MainChart  data={this.props.valutes} />
    }else{
      chartPlaceholder = <p>Getting data for chart creating...</p>
    }

    return (
      <div className="App">
        <h2>Now is {this.state.date.toLocaleTimeString()}.</h2>
        {titlePlaceholder}
        {chartPlaceholder}
        <div className="valutes-list">
          <table>
            <thead>
              <tr>
                <td>#</td>
                <td>Query datetime</td>
                <td>EUR</td>
                <td>USD</td>
              </tr>
            </thead>
            <tbody>
                {this.props.valutes.map((val, i) => {
                  return <tr>
                    <td>{i + 1}</td>
                    <td>{val.datetime}</td>
                    <td>{val.EUR}</td>
                    <td>{val.USD}</td>
                  </tr> 
                })}
            </tbody> 
          </table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    valutes: state.valutes
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchData: url => dispatch(valutesFetchData(url))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App)