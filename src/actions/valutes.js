export function valutesFetchDataSuccess(valutes) {
    return {
        type: "VALUTES_FETCH_DATA_SUCCESS",
        valutes
    }
}

export function valutesFetchData(url) {
    return (dispatch) => {
        fetch(url).then(function(response) {
            return response.text();
        }).then(function(valutes) {
            dispatch(valutesFetchDataSuccess(JSON.parse(valutes)));
        });
    }
}